<?php

use EmbedVideo\VideoService;
use MediaWiki\MediaWikiServices;

/**
 * EmbedVideo
 * EmbedVideo Hooks
 *
 * @license MIT
 * @package EmbedVideo
 * @link    https://www.mediawiki.org/wiki/Extension:EmbedVideo
 **/

class EmbedVideoHooks {
	/**
	 * Temporary storage for the current service object.
	 *
	 * @var object
	 */
	static private $service;

	/**
	 * Description Parameter
	 *
	 * @var string
	 */
	static private $description = false;

	/**
	 * Alignment Parameter
	 *
	 * @var string
	 */
	static private $alignment = false;

	/**
	 * Alignment Parameter
	 *
	 * @var string
	 */
	static private $vAlignment = false;

	/**
	 * Container Parameter
	 *
	 * @var string
	 */
	static private $container = false;

	/**
	 * Hook to setup defaults.
	 *
	 * @access public
	 * @return void
	 */
	public static function onExtension() {
		global $wgMediaHandlers, $wgFileExtensions;

		$config = MediaWikiServices::getInstance()->getConfigFactory()->makeConfig('main');

		if ($config->get('EmbedVideoEnableAudioHandler')) {
			$wgMediaHandlers['application/ogg']		= 'EmbedVideo\AudioHandler';
			$wgMediaHandlers['audio/flac']			= 'EmbedVideo\AudioHandler';
			$wgMediaHandlers['audio/ogg']			= 'EmbedVideo\AudioHandler';
			$wgMediaHandlers['audio/mpeg']			= 'EmbedVideo\AudioHandler';
			$wgMediaHandlers['audio/mp4']			= 'EmbedVideo\AudioHandler';
			$wgMediaHandlers['audio/wav']			= 'EmbedVideo\AudioHandler';
			$wgMediaHandlers['audio/webm']			= 'EmbedVideo\AudioHandler';
			$wgMediaHandlers['audio/x-flac']		= 'EmbedVideo\AudioHandler';
		}

		if ($config->get('EmbedVideoAddFileExtensions')) {
			$wgFileExtensions[] = 'flac';
			$wgFileExtensions[] = 'mp3';
			$wgFileExtensions[] = 'oga';
			$wgFileExtensions[] = 'ogg';
			$wgFileExtensions[] = 'wav';
		}
	}

	/**
	 * Sets up this extension's parser functions.
	 *
	 * @access public
	 * @param  Parser $parser	Parser object passed as a reference.
	 * @return boolean	true
	 */
	public static function onParserFirstCallInit(Parser &$parser) {
		$parser->setFunctionHook("youtube", "EmbedVideoHooks::parseYT");

		return true;
	}

	/**
	 * Embeds a video from YouTube.
	 *
	 * @access public
	 *
	 * @param Parser $parser	Parser
	 * @param  ?string $id 	Identifier Code or URL for the video on the service.
	 * @param  ?string $dimensions	[Optional] Dimensions of video
	 * @param  ?string $alignment	[Optional] Horizontal Alignment of the embed container.
	 * @param  ?string $description	[Optional] Description to show
	 * @param  ?string $urlArgs	[Optional] Extra URL Arguments
	 *
	 * @return array	Encoded representation of input params (to be processed later)
	 */
	public static function parseYT(
		Parser $parser,
		string $id = null,
		string $dimensions = null,
		string $alignment = null,
		string $description = null,
		string $urlArgs = null
	) {
		self::resetParameters();

		$id				= trim($id);
		$alignment		= trim($alignment);
		$description	= trim($description);
		$dimensions		= trim($dimensions);
		$urlArgs		= trim($urlArgs);
		$width			= null;
		$height			= null;

		// I am not using $parser->parseWidthParam() since it can not handle height only.  Example: x100
		if (stristr($dimensions, 'x')) {
			$dimensions = strtolower($dimensions);
			list( $width, $height ) = explode('x', $dimensions);
		} elseif (is_numeric($dimensions)) {
			$width = $dimensions;
		}

		/************************************/
		/* Error Checking                   */
		/************************************/
		if (!$id) {
			return self::error('missingparams', $id);
		}

		self::$service = VideoService::newFromName('youtube');

		// Let the service automatically handle bad dimensional values.
		self::$service->setWidth($width);

		self::$service->setHeight($height);

		// If the service has an ID pattern specified, verify the id number.
		if (!self::$service->setVideoID($id)) {
			return self::error('id', $id);
		}

		if (!self::$service->setUrlArgs($urlArgs)) {
			return self::error('urlargs', $urlArgs);
		}

		if (!is_null($parser)) {
			self::setDescription($description, $parser);
		} else {
			self::setDescriptionNoParse($description);
		}

		self::setContainer('frame');

		if (!self::setAlignment($alignment)) {
			return self::error('alignment', $alignment);
		}

		/************************************/
		/* HTML Generation                  */
		/************************************/
		$html = self::$service->getHtml();
		if (!$html) {
			return self::error('unknown');
		}

		$html = self::generateWrapperHTML($html);

		if ($parser) {
			// dont call this if parser is null (such as in API usage).
			$out = $parser->getOutput();
			$out->addModules('ext.embedVideo');
			$out->addModuleStyles('ext.embedVideo.styles');
		}

		return [
			$html,
			'noparse' => true,
			'isHTML' => true
		];
	}

	/**
	 * Generate the HTML necessary to embed the video with the given alignment
	 * and text description
	 *
	 * @access private
	 * @param  string	[Optional] Horizontal Alignment
	 * @param  string	[Optional] Description
	 * @param  string  [Optional] Additional Classes to add to the wrapper
	 * @return string
	 */
	private static function generateWrapperHTML($html, $description = null, $addClass = null) {
		$classString = "embedvideo";
		$styleString = "";
		$innerClassString = "embedvideowrap";
		$outerClassString = "embedvideo ";

		if (self::getContainer() == 'frame') {
			$classString .= " thumbinner";
		}

		if (self::getAlignment() !== false) {
			$outerClassString .= " ev_" . self::getAlignment() . " ";
			$styleString .= " width: " . (self::$service->getWidth() + 6) . "px;";
		}

		if (self::getVerticalAlignment() !== false) {
			$outerClassString .= " ev_" . self::getVerticalAlignment() . " ";
		}

		if ($addClass) {
			$classString .= " " . $addClass;
			$outerClassString .= $addClass;
		}

		$html = "<div class='thumb $outerClassString' style='width: " . (self::$service->getWidth() + 8) . "px;'><div class='" . $classString . "' style='" . $styleString . "'><div class='" . $innerClassString . "' style='width: " . self::$service->getWidth() . "px;'>{$html}</div>" . (self::getDescription() !== false ? "<div class='thumbcaption'>" . self::getDescription() . "</div>" : null) . "</div></div>";

		return $html;
	}

	/**
	 * Return the alignment parameter.
	 *
	 * @access public
	 * @return mixed	Alignment or false for not set.
	 */
	private static function getAlignment() {
		return self::$alignment;
	}

	/**
	 * Set the align parameter.
	 *
	 * @access private
	 * @param  string	Alignment Parameter
	 * @return boolean	Valid
	 */
	private static function setAlignment($alignment) {
		if (!empty($alignment) && ($alignment == 'left' || $alignment == 'right' || $alignment == 'center' || $alignment == 'inline')) {
			self::$alignment = $alignment;
		} elseif (!empty($alignment)) {
			return false;
		}
		return true;
	}

	/**
	 * Return the valignment parameter.
	 *
	 * @access public
	 * @return mixed	Vertical Alignment or false for not set.
	 */
	private static function getVerticalAlignment() {
		return self::$vAlignment;
	}

	/**
	 * Return description text.
	 *
	 * @access private
	 * @return mixed	String description or false for not set.
	 */
	private static function getDescription() {
		return self::$description;
	}

	/**
	 * Set the description.
	 *
	 * @access private
	 * @param  string $description	Description
	 * @param  \Parser $parser	Mediawiki Parser object
	 * @return void
	 */
	private static function setDescription($description, \Parser $parser) {
		self::$description = (!$description ? false : $parser->recursiveTagParse($description));
	}

	/**
	 * Set the description without using the parser
	 *
	 * @param string	Description
	 */
	private static function setDescriptionNoParse($description) {
		self::$description = (!$description ? false : $description);
	}

	/**
	 * Return container type.
	 *
	 * @access private
	 * @return mixed	String container type or false for not set.
	 */
	private static function getContainer() {
		return self::$container;
	}

	/**
	 * Set the container type.
	 *
	 * @access private
	 * @param  string	Container
	 * @return boolean	Success
	 */
	private static function setContainer($container) {
		if (!empty($container) && ($container == 'frame')) {
			self::$container = $container;
		} elseif (!empty($container)) {
			return false;
		}
		return true;
	}

	/**
	 * Reset parameters between parses.
	 *
	 * @access private
	 * @return void
	 */
	private static function resetParameters() {
		self::$description	= false;
		self::$alignment	= false;
		self::$container	= false;
	}

	/**
	 * Error Handler
	 *
	 * @access private
	 * @param  string	[Optional] Error Type
	 * @param  mixed	[...] Multiple arguments to be retrieved with func_get_args().
	 * @return array	Printable Error Message
	 */
	private static function error($type = 'unknown') {
		$arguments = func_get_args();
		array_shift($arguments);

		$message = wfMessage('error_embedvideo_' . $type, $arguments)->escaped();

		return [
			"<div class='errorbox'>{$message}</div>",
			'noparse' => true,
			'isHTML' => true
		];
	}
}
