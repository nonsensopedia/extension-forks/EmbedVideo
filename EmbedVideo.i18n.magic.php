<?php
/**
 * EmbedVideo
 * EmbedVideo Magic Words
 *
 * @license MIT
 * @package EmbedVideo
 * @link    https://www.mediawiki.org/wiki/Extension:EmbedVideo
 **/

$magicWords = [];

$magicWords['en'] = [
	'youtube' => [0, 'youtube'],
];
